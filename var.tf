variable "vmcount" {
    default = "1"    
}

variable "prox_api"{
  default = ":8006/api2/json"
}

variable "hostname" {
  default  = "lnx-server"
}

variable "clone_name" {
  default = "ubuntu-2004"

}

variable "pool_name" {
  default = "itnoobs-core"
}

variable "rg" {
  default = "rg-terraform-ause"
}

variable "keyvault"{
  default = "itnoobs-keyvault"
}

variable "node_name" {
  default = "node1"
}

variable "domain_name" {
  default = ".itnoobs.local"
}