# Terraform state file keeps track of all changes applied and any resources imported.
# It's kept locally by default, however very useful to use remote tfstate in a team environment 
# so that state changes are recorded and visible for everyone.

terraform {   
 backend "azurerm" {     
 resource_group_name  = "rg-terraform-ause"
 storage_account_name  = "itnoobs"   
 container_name        = "tfstate"    
 
 # This is the name of the tfstate file that will be created during first initialization
 key                   = "itnoobs-new.tfstate" 

  } 
}