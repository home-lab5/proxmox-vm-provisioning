provider "proxmox" {
  
    pm_api_url = "https://${var.node_name}${var.domain_name}${var.prox_api}"
    pm_password = data.azurerm_key_vault_secret.keyvault.0.value
    pm_user = "root@pam"
    pm_tls_insecure = true

}


resource "proxmox_vm_qemu" "prox-vm" {

  name = "${var.hostname}1"
  desc = var.hostname
  target_node = var.node_name
  count = var.vmcount
  full_clone = true
  clone = var.clone_name

  pool = var.pool_name

  cores = 8
  sockets = 2
  vcpus = 4
  memory = 2048
  boot =  "c"
  bootdisk = "virtio0"
  
  scsihw = "virtio-scsi-pci"
  
  onboot = false
  agent =  1
  cpu = "kvm64"
  numa = true
  hotplug = "network,disk,cpu,memory"
  
  network {

    model = "virtio"
    bridge = "vmbr4"
    tag = 2
    queues = 0
    rate = 0
    macaddr = ""
  }
  disk {

    type = "virtio"
    storage = "vm_pool0"
    size = "10G"
    file        = ""
    format      = ""
    media       = ""
    slot        = 0
    ssd         = true
    volume      = ""
  }

  os_type = "cloud-init"
 
  # This is to avoid terraform plan detecting changes that are not handled correctly by the plugin
  lifecycle {
    ignore_changes = [

      network[0],
      disk[0],
      balloon,
      ciuser,
      disk_gb,
      id,
      ipconfig0,
      memory,
      name,
      nameserver,
      qemu_os,
      searchdomain,
      ssh_host,
      ssh_port,
      sshkeys      
    
    ]
  }
  
# Reboot VM to fix DNS entry and remove cloud-init drive
  provisioner "local-exec" {
      
      working_dir = "./ansible/"
      command = "ansible-playbook -i ${var.node_name}${var.domain_name}, phase1.yaml --extra-vars 'vm=${self.name}'"
      
  }
  # Install QEMU agent and create CPU hotplug config file if not there, then reboot
  provisioner "local-exec" {
      
      working_dir = "./ansible/"
      command = "  ansible-playbook --flush-cache -i ${self.name}${var.domain_name}, phase2.yaml"
  }

}


