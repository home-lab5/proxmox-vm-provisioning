# A local value assigns a name to an expression, allowing it to be used multiple times within a module 
# without repeating it, used to loop through multiple Key Vault secrets

locals {
  secret_names = ["proxmoxpass"] #, "sshuser", "sshpubkey"]
}

# Actual Key Vault name - this does not need to be imported by Terraform
data "azurerm_key_vault" "keyvault" {
  name                = var.keyvault
  resource_group_name = var.rg
}

# Key Vault Secrets
data "azurerm_key_vault_secret" "keyvault" {
  count = length(local.secret_names)
  # Pulls value based on secret name set by count index
  name =  element(local.secret_names, count.index)
  key_vault_id = data.azurerm_key_vault.keyvault.id
  

}

