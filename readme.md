# Provision Multiple VMs on Proxmox

**This repository will no longer be updated/maintained.** Code was (last) tested successfully with Proxmox VE 6.3.3, Terraform 0.14.0, AzureRM 2.0.0 and Telmate Proxmox 2.6.5.

![versions](/images/tf-version.png)

Although it creates the resources without any issues, there are (old and) new idempotency bugs with the latest version of provider and after spending some time and trying the good old _lifecycle_ trick I was not able to make it work reliably. **I created a simple [Ansible playbook](https://gitlab.com/itnoobs-automation/ansible/proxmox-vm) that looks after VM creation and management instead.**

![idempotency](/images/idempotency.png)

This project uses Terraform and Ansible to provision multiple VMs on Proxmox host.
For more details and a few video of Terraform & Ansible in action see my blog [post](https://itnoobs.net/automation/proxmox-automation).

## Prerequisites

- Internal DNS and DHCP
- Proxmox VE 6.x host (tested on PVE 6.1-8)
- Linux control host - to run Terraform, Ansible and SSH connection to Proxmox and virtual machines
- SSH with key pair authentication to Proxmox host and guest VMs - [Generate SSH key pair](#generate-ssh-key-pair)
- Terraform binaries - i.e. [Download](https://www.terraform.io/downloads.html) binary and copy to `/usr/bin/`
- Ansible - i.e. `sudo apt install ansible`
- Cloud-init template - [Create cloud-init template](#create-cloud-init-template)
- Terraform provider and pluging binaries - [Install Terraform provider](#install-terraform-provider-and-plugin)
- If using Azure Key Vault and Storage:
  - Install Azure CLI - i.e. [Install Azure CLI with apt](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-apt?view=azure-cli-latest)
  - Create Azure Key Vault and Secret
  - Create Azure Storage account and Container

## Getting Started

### Make sure all prerequisites are met or configured correctly

- Clone or download this repository
  - `git clone git@gitlab.com:itnoobs-automation/terraform/proxmox-vm-provisioning.git`
  - `wget https://gitlab.com/itnoobs-automation/terraform/proxmox-vm-provisioning/-/archive/master/proxmox-vm-provisioning-master.zip`
- Run `az login` and sign into your account
- Change `pm_api_url` value in main.tf to match your environment
- Change directory, `cd proxmox-template`
- Run
  - `terraform init` There should be no errors
  - `terraform plan -out plan` Review, change and re-run as required
  - `terraform apply plan`

## Generate SSH key pair

- Generate key pair `ssh-keygen`
- Add public key to the remote host `ssh-copy-id -i ~/.ssh/id_rsa.pub <username>@<hostname>`
- Add private key to SSH session ```eval `ssh-agent` && ssh-add -i ~/.ssh/id_rsa```
- Log in without password authentication `ssh <username>@<hostname>`

## Create cloud-init template

### Official Proxmox [documentation](https://pve.proxmox.com/wiki/Cloud-Init_Support)

Create cloud-init template from latest Ubuntu 20.04 image. Alternatively you can use my [Ansible playbook](https://gitlab.com/itnoobs-automation/ansible/proxmox-template)

- copy the link to the latest [Ubuntu cloud-init image file](http://cloud-images.ubuntu.com/focal/current/)
- SSH into your Proxmox host
- `wget http://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img`
- `qm importdisk 9000 focal-server-cloudimg-amd64.img local-lvm`
- `qm create 9000 --memory 2048 --net0 virtio,bridge=vmbr0 --sockets 2 --cores 8 --vcpu 4 --numa 1 -hotplug network,disk,cpu,memory --agent 1 --pool Templates --name cloud-init-focal --ostype l26`
- `qm set 9000 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-9000-disk-0`
- `qm set 9000 --ide2 local-lvm:cloudinit`
- `qm set 9000 --boot c --bootdisk virtio0`
- `qm set 9000 --serial0 socket`
- `qm template 9000`
- Add username, password, SSH public key, DNS domain name, DNS servers and IP config via web portal

## Install Terraform provider and plugin

With latest version of Terraform, manual compiling of the Telmate plugin is no longer required.
More info can be found [here](https://github.com/Telmate/terraform-provider-proxmox/blob/master/docs/guides/installation.md).

## Misc

[Ansible playbook](https://gitlab.com/itnoobs-automation/ansible/proxmox-vm) as a replacement to this project for creating VMs on Proxmox.
